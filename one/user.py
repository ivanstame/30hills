class User:

    def __init__(self, id, first_name, last_name, age, gender, friends_ids):
        self.id = id
        self.first_name = first_name
        self.last_name = last_name
        self.age = age
        self.gender = gender
        self.friends = {}
        self.friends_ids = friends_ids.strip(' \t\n\r').replace(' ', '')

    def add_friends(self, users):
        # print 'adding friends', users
        self.friends = users

    def is_in_friends(self, id):
        if str(id) in self.friends:
            return True
        return False

    def list_friends(self):
        print 'user', self.id, self.first_name
        for f in self.friends:
            print self.friends[f].id

    @staticmethod
    def deserialize(line):
        attrs = line.split(';') # maybe add a delim as a variable instead of hardcoding it...
        # 0:id, 1:first_name, 2:last_name, 3:age, 4:gender, 5:friends
        return User(attrs[0], attrs[1], attrs[2], attrs[3], attrs[4], attrs[5])
