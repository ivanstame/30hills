from graph import Graph
from collections import defaultdict
from heapq import *

class Datastore():

    def __init__(self):
        self.store = {}

    def add(self, id, item):
        self.store[id] = item

    def get_dict(self, ids):
        res = {}

        for id in ids.split(','):
            if id in self.store:
                res[id] = self.store[id]

        return res

    # We are not using any loops to check if two persons are friends because
    # that would give us O(n) time complexity, instead we have structured our
    # data in that way that we can lookup very fast.
    # As the manuals and people says, look up(in keyword) has a time
    # complexity of O(1) for dicts in python.
    def is_friend(self, id1, id2):

        id1 = str(id1)
        id2 = str(id2)

        # check the first case
        if id1 in self.store:
            if self.store[id1].is_in_friends(id2):
                return True

        # and the second case
        if id2 in self.store:
            if self.store[id2].is_in_friends(id1):
                return True

        return False

    def make_graph(self):
        self.graph = Graph()
        for user in self.store:
            self.graph.add_node(user)
            for friend in self.store[user].friends:
                self.graph.add_connection(user, friend, 1)

    def dijsktra(self, graph, start):
        visited = { start: 0 }
        path = {}

        nodes = set(graph.nodes)

        while nodes:
            min_node = None
            for node in nodes:
                if node in visited:
                    if min_node is None:
                        min_node = node
                    elif visited[node] < visited[min_node]:
                        min_node = node

            if min_node is None:
              break

            nodes.remove(min_node)
            current_weight = visited[min_node]

            for conn in graph.connections[min_node]:
                weight = current_weight + graph.weights[(min_node, conn)]
                if conn not in visited or weight < visited[conn]:
                    visited[conn] = weight
                    path[conn] = min_node

        return visited, path


    # using the graph and calling dijsktra algorithm
    # to get the path and distances
    def shortest_distance(self, graph, start, end):
        v, p = self.dijsktra(graph, start)
        path = []
        while True:
            path.append(end)
            if end == start: break
            end = p[end]
            
        path.reverse()
        path.pop(0)
        return path
        
        
