import sys
from datastore import Datastore
from user import User

def main():
    ds = Datastore()
    read_file(ds)
    ds.make_graph()
    print ds.shortest_distance(ds.graph, '10', '1')

def read_file(ds):
    handler = open("data.txt", "r")
    lines = []

    # read file line by line
    for line in handler:
        if len(line.strip()) <= 0:
            print 'line is just an empty string'
            continue
        else:
            lines.append(line)

    deserialize(lines, ds)

def deserialize(lines, ds):
    # read user data and populate our store
    for line in lines:
        user = User.deserialize(line)
        ds.add(user.id, user)

    associate_friends(ds)

def associate_friends(ds):
    for k in ds.store:
        friends = ds.get_dict(ds.store[k].friends_ids)
        ds.store[k].add_friends(friends)

if __name__ == '__main__':
    sys.exit(main())
