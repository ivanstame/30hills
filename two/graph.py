from collections import defaultdict

class Graph:

    def __init__(self):
        self.nodes = set()
        self.connections = defaultdict(list)
        self.weights = {}

    def add_node(self, value):
        self.nodes.add(value)

    def add_connection(self, from_node, to_node, weight):
        self.connections[from_node].append(to_node)
        self.connections[to_node].append(from_node)
        self.weights[(from_node, to_node)] = weight
