from graph import Graph
from collections import defaultdict
from heapq import *
import sqlite3
from user import User
import sys

class Datastore():

    def __init__(self):
        self.store = {}
        self.conn = sqlite3.connect('dev.db')
        self.c = self.conn.cursor()

    def add(self, id, item):
        self.store[id] = item
        
    def get_user(self, id):
        res = self.c.execute('SELECT * FROM users where id=?',id)
        print res
        return User.deserialize(self, res.fetchone())

    # extract ids from id list, and fetch the data from
    # internal storage, and populate the user object with
    # his friends
    def get_dict(self, ids):
        res = {}

        for id in ids.split(','):
            if id in self.store:
                res[id] = self.store[id]

        return res
        
    # not scalable at all, but it works for this demo :)
    def read_all_users(self):
        for row in self.c.execute('SELECT * FROM users'):
            user = User.deserialize(self, row)
            self.add(user.id, user)
        self.associate_friends()
        
    def associate_friends(self):
        for k in self.store:
            friends = self.get_dict(self.store[k].friends_ids)
            self.store[k].add_friends(friends)

    # We are not using any loops to check if two persons are friends because
    # that would give us O(n) time complexity, instead we have structured our
    # data in that way that we can lookup very fast.
    # As the manuals and people says, look up(in keyword) has a time
    # complexity of O(1) for dicts in python.
    def is_friend(self, id1, id2):

        id1 = str(id1)
        id2 = str(id2)

        # check the first case
        if id1 in self.store:
            if self.store[id1].is_in_friends(id2):
                return True

        # and the second case
        if id2 in self.store:
            if self.store[id2].is_in_friends(id1):
                return True

        return False

    # map all the users and their firends
    def make_graph(self):
        self.graph = Graph()
        for user in self.store:
            self.graph.add_node(user)
            for friend in self.store[user].friends:
                self.graph.add_connection(user, friend, 1)

    def dijsktra(self, graph, start):
        visited = { start: 0 }
        path = {}

        nodes = set(graph.nodes)

        while nodes:
            min_node = None
            for node in nodes:
                if node in visited:
                    if min_node is None:
                        min_node = node
                    elif visited[node] < visited[min_node]:
                        min_node = node

            if min_node is None:
              break

            nodes.remove(min_node)
            current_weight = visited[min_node]

            for conn in graph.connections[min_node]:
                weight = current_weight + graph.weights[(min_node, conn)]
                if conn not in visited or weight < visited[conn]:
                    visited[conn] = weight
                    path[conn] = min_node

        return visited, path


    # using the graph and calling dijsktra algorithm
    # to get the path and distances
    def shortest_distance(self, graph, start, end):
        v, p = self.dijsktra(graph, start)
        path = []
        while True:
            path.append(end)
            if end == start: break
            end = p[end]
            
        path.reverse()
        path.pop(0)
        return path
        
        
