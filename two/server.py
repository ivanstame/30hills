from flask import Flask, jsonify, render_template, session, redirect
from datastore import Datastore
from user import User
import json

app = Flask(__name__)

@app.route('/')
def home():
    return render_template('index.html')
    
@app.route('/partials/profile')
def partial_profile():
    return render_template('profile.html')
    
@app.route('/partials/home')
def partial_home():
    return render_template('home.html')
    
@app.route('/users')
def users():
    temp = "[ "
    for id in ds.store:
        temp = temp + to_JSON(ds.store[id])
    temp = temp + "]"
    return temp
    
@app.route('/user/<id>/friends')
def friends(id):
    temp = "[ "
    for idx in ds.store:
        if int(id) == idx:
            print ds.store[int(id)].friends
            for user in ds.store[int(id)].friends:
                temp = temp + to_JSON(user)
    temp = temp + "]"
    return temp
    
@app.route('/user/<id>')
def user(id):
    temp = to_JSON(ds.store[id])
    return temp


def to_JSON(obj):
    return json.dumps(obj, default=lambda o: o.__dict__, sort_keys=True, indent=4)
        
if __name__ == '__main__':
    ds = Datastore()
    ds.read_all_users()
    ds.make_graph()
    app.debug = True
    app.secret_key = 'A0Zr98jdsfdsfsdfdsN]LWX/,?RT'
    app.run(host='0.0.0.0')
