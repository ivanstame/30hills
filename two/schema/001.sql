CREATE TABLE users (
    "id" INTEGER,
    "first_name" TEXT,
    "last_name" TEXT,
    "age" INTEGER,
    "gender" TEXT
);

CREATE TABLE "friends" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT,
    "user_id" INTEGER,
    "friend_id" INTEGER
);


INSERT INTO users (first_name, last_name, age, gender) VALUES ("Paul", "Crowe", 28, "male");
INSERT INTO users (first_name, last_name, age, gender) VALUES ("Rob", "Fitz", 23, "male");
INSERT INTO users (first_name, last_name, age, gender) VALUES ("Ben", "O'Carolan", 0, "male");
INSERT INTO users (first_name, last_name, age, gender) VALUES ("Victor", "", 28, male, "3");
INSERT INTO users (first_name, last_name, age, gender) VALUES ("Peter", "Mac", 29, "male");
INSERT INTO users (first_name, last_name, age, gender) VALUES ("John", "Barry", 18, "male");
INSERT INTO users (first_name, last_name, age, gender) VALUES ("Sarah", "Lane", 30, "female");
INSERT INTO users (first_name, last_name, age, gender) VALUES ("Susan", "Downe", 28, "female");
INSERT INTO users (first_name, last_name, age, gender) VALUES ("Jack", "Stam", 28, "male");
INSERT INTO users (first_name, last_name, age, gender) VALUES ("Amy", "Lane", 24, "female");
INSERT INTO users (first_name, last_name, age, gender) VALUES ("Sandra", "Phelan", 28, "female");
INSERT INTO users (first_name, last_name, age, gender) VALUES ("Laura", "Murphy", 33, "female");
INSERT INTO users (first_name, last_name, age, gender) VALUES ("Lisa", "Daly", 13, "female");
INSERT INTO users (first_name, last_name, age, gender) VALUES ("Mark", "Johnson", 28, "male");
INSERT INTO users (first_name, last_name, age, gender) VALUES ("Seamus", "Crowe", 24, "male");
INSERT INTO users (first_name, last_name, age, gender) VALUES ("Daren", "Slater", 28, "male");
INSERT INTO users (first_name, last_name, age, gender) VALUES ("Dara", "Zoltan", 48, "female");
INSERT INTO users (first_name, last_name, age, gender) VALUES ("Marie", "D", 28, "female");
INSERT INTO users (first_name, last_name, age, gender) VALUES ("Catriona", "Long", 28, "female");
INSERT INTO users (first_name, last_name, age, gender) VALUES ("Katy", "Couch", 28, "female");
