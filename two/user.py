class User:

    def __init__(self, id, first_name, last_name, age, gender, friends_ids):
        self.id = id
        self.first_name = first_name
        self.last_name = last_name
        self.age = age
        self.gender = gender
        self.friends = {}
        self.friends_ids = friends_ids.strip(' \t\n\r').replace(' ', '')

    def add_friends(self, users):
        self.friends = users

    def is_in_friends(self, id):
        if str(id) in self.friends:
            return True
        return False

    def list_friends(self):
        print 'user', self.id, self.first_name
        for f in self.friends:
            print self.friends[f].id

    @staticmethod
    def deserialize(ds, line):
        friend_ids = ""
        temp_list = []
        for row in ds.c.execute('SELECT * FROM friends where user_id=?', str(line[0])):
            temp_list.append(row[2])
        friend_ids = ",".join(map(str, temp_list))
        return User(line[0], line[1], line[2], line[3], line[4], friend_ids)
